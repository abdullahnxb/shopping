import classes from "./Header.module.css";
import mealsImg from "../../assets/meals.jpg";
import HeaderCartButton from "./HeaderCartButton";

const Header = (props) => {
  return (
    <>
      <header className={classes.header}>
        <h1>React Meals</h1>
        <HeaderCartButton onCartClick={props.onCartShow} />
      </header>
      <div className={classes["main-image"]}>
        <img src={mealsImg} alt="Food Table" />
      </div>
    </>
  );
};

export default Header;
