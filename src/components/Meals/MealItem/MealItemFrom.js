import { useRef, useState } from "react";
import Input from "../../UI/Input/Input";
import classes from "./MealItemForm.module.css";

const MealItemForm = (props) => {
  const [formIsValid, setFormIsValid] = useState(true);
  const inputRef = useRef();
  const onSubmitFormHandler = (event) => {
    event.preventDefault();

    const enteredAmount = inputRef.current.value; //always get string
    const enteredAmountNumber = +enteredAmount;

    if (
      enteredAmount.trim().length === 0 ||
      enteredAmountNumber < 1 ||
      enteredAmountNumber > 5
    ) {
      setFormIsValid(false);
      return;
    }

    setFormIsValid(true);
    props.onAddCart(enteredAmountNumber);
  };
  return (
    <form className={classes.form} onSubmit={onSubmitFormHandler}>
      <Input
        ref={inputRef}
        label="Amount"
        input={{
          id: "amount_" + props.id,
          type: "number",
          min: 1,
          max: 5,
          step: 1,
          defaultValue: 1,
        }}
      />
      <button>+ Add</button>
      {!formIsValid && <small>Please enter correct amount [1-5]</small>}
    </form>
  );
};

export default MealItemForm;
