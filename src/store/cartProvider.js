import { useReducer } from "react";
import CartContext from "./cartContext";

const CartProvider = (props) => {
  const defaultCartState = {
    items: [],
    totalAmount: 0,
  };

  const cartReducer = (state, action) => {
    if (action.type === "ADD") {
      const updatedCartAmount =
        state.totalAmount + action.item.price * action.item.amount;

      const existingCartItemIndex = state.items.findIndex(
        (item) => item.id === action.item.id
      );

      const existingCartItem = state.items[existingCartItemIndex];

      let updatedCartItems;

      if (existingCartItem) {
        const updatedCartItem = {
          ...existingCartItem,
          amount: existingCartItem.amount + action.item.amount,
        };
        updatedCartItems = [...state.items];
        updatedCartItems[existingCartItemIndex] = updatedCartItem;
      } else {
        updatedCartItems = state.items.concat(action.item);
      }

      return {
        items: updatedCartItems,
        totalAmount: updatedCartAmount,
      };
    }

    if (action.type === "REMOVE") {
      const existingCartItemIndex = state.items.findIndex(
        (item) => item.id === action.id
      );

      const existingCartItem = state.items[existingCartItemIndex];
      const updatedCartAmount = state.totalAmount - existingCartItem.price;

      let updatedCartItems;

      if (existingCartItem.amount === 1) {
        updatedCartItems = state.items.filter((item) => item.id !== action.id);
      } else {
        const updatedCartItem = {
          ...existingCartItem,
          amount: existingCartItem.amount - 1,
        };
        updatedCartItems = [...state.items];
        updatedCartItems[existingCartItemIndex] = updatedCartItem;
      }

      return {
        items: updatedCartItems,
        totalAmount: updatedCartAmount,
      };
    }

    return defaultCartState;
  };

  const [cartState, dispatchCartActions] = useReducer(
    cartReducer,
    defaultCartState
  );

  const addItemToCartHandler = (item) => {
    dispatchCartActions({ type: "ADD", item: item });
  };

  const removeItemFromCarthandler = (id) => {
    dispatchCartActions({ type: "REMOVE", id: id });
  };

  const cartContextValue = {
    items: cartState.items,
    totalAmount: cartState.totalAmount,
    addItem: addItemToCartHandler,
    removeItem: removeItemFromCarthandler,
  };

  return (
    <CartContext.Provider value={cartContextValue}>
      {props.children}
    </CartContext.Provider>
  );
};

export default CartProvider;
